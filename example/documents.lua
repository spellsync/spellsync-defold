local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local function open()
    spellsync.documents.open({ type = "PLAYER_PRIVACY_POLICY" })
end

local function fetch()
    spellsync.documents.fetch({ type = "PLAYER_PRIVACY_POLICY", format = "TXT" }, function(document)
        utils.to_log("Documents fetch")
        pprint("Documents fetch result:", document)
    end)
end

local M = {
    { name = "Open", callback = open },
    { name = "Fetch", callback = fetch },
}

spellsync.documents.callbacks.open = function()
    utils.to_console("Document open")
end
spellsync.documents.callbacks.close = function()
    utils.to_console("Document close")
end
spellsync.documents.callbacks.fetch = function(document)
    utils.to_console("Document fetch", document)
end
spellsync.documents.callbacks.error_fetch = function(error)
    utils.to_console("Document fetch error:", error)
end

return M