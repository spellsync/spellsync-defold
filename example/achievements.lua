local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local function groups_list()
    utils.to_log("Groups list:", spellsync.achievements.groups_list())
end

local function list()
    utils.to_log("List:", spellsync.achievements.list())
end

local function unlocked_list()
    utils.to_log("Unlocked list:", spellsync.achievements.unlocked_list())
end

local function unlock()
    spellsync.achievements.unlock({ tag = "my_achiv" }, function(result)
        utils.to_log("Achievements unlock:", result)
    end)
end

local function set_progress()
    spellsync.achievements.set_progress({ tag = "my_achiv", progress = 25 }, function(result)
        utils.to_log("Achievements set progress:", result)
    end)
end

local function has()
    utils.to_log(spellsync.achievements.has("my_achiv"))
end

local function get_progress()
    utils.to_log(spellsync.achievements.get_progress(1960))
end

local function open()
    spellsync.achievements.open(function()
        utils.to_log("Achievements open")
    end)
end

local function fetch()
    spellsync.achievements.fetch(function(achievements)
        utils.to_log("Achievements fetch:", achievements)
    end)
end

local M = {
    { name = "Groups list", callback = groups_list },
    { name = "List", callback = list },
    { name = "Unlocked list", callback = unlocked_list },
    { name = "Unlock", callback = unlock },
    { name = "Set progress", callback = set_progress },
    { name = "Has", callback = has },
    { name = "Get progress", callback = get_progress },
    { name = "Open", callback = open },
    { name = "Fetch", callback = fetch },
}

spellsync.achievements.callbacks.unlock = function(achievement)
    utils.to_console("achievement unlock:", achievement)
end
spellsync.achievements.callbacks.error_unlock = function(error)
    utils.to_console("achievement unlock error:", error)
end
spellsync.achievements.callbacks.progress = function(achievement)
    utils.to_console("achievement progress:", achievement)
end
spellsync.achievements.callbacks.error_progress = function(error)
    utils.to_console("achievement progress error:", error)
end
spellsync.achievements.callbacks.open = function()
    utils.to_console("open achievements")
end
spellsync.achievements.callbacks.close = function()
    utils.to_console("close achievements")
end
spellsync.achievements.callbacks.fetch = function(result)
    utils.to_console("fetch achievements:", result)
end
spellsync.achievements.callbacks.error_fetch = function(error)
    utils.to_console("fetch achievements error:", error)
end

return M