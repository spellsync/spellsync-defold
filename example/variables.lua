local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local function fetch()
    spellsync.variables.fetch(function()
        utils.to_log("variables fetch")
    end)
end

local function get()
    local var = spellsync.variables.get("var")
    if var == nil then
        var = "nil"
    end
    utils.to_log("variables get 'var': " .. tostring(var))
end

local function has()
    utils.to_log("variables has 'var': " .. tostring(spellsync.variables.has("var")))
end

local function type()
    utils.to_log("variables type 'var': " .. tostring(spellsync.variables.type("var")))
end

local M = {
    { name = "Fetch", callback = fetch },
    { name = "Get", callback = get },
    { name = "Has", callback = has },
    { name = "Type", callback = type },
}

spellsync.variables.callbacks.fetch = function()
    utils.to_console("Variables fetch")
end
spellsync.variables.callbacks.error = function(error)
    utils.to_console("Variables fetch error:", error)
end

return M