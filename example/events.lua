local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local event_tag = "my_event"

local function join()
    spellsync.events.join({ tag = event_tag }, function(result)
        utils.to_log("Join event:", result)
    end)
end

local function list()
    utils.to_log("List events:", spellsync.events.list())
end

local function active_list()
    utils.to_log("Active list events:", spellsync.events.active_list())
end

local function get_event()
    utils.to_log("Get event:", spellsync.events.get_event(event_tag))
end

local function has()
    utils.to_log("Event has:", spellsync.events.has(event_tag))
end

local function is_joined()
    utils.to_log("Is joined:", spellsync.events.is_joined(event_tag))
end

local M = {
    { name = "Join", callback = join },
    { name = "List", callback = list },
    { name = "Active list", callback = active_list },
    { name = "Get event", callback = get_event },
    { name = "Has", callback = has },
    { name = "Is joined", callback = is_joined },
}

spellsync.events.callbacks.join = function(reward)
    utils.to_console("Join event:", reward)
end
spellsync.events.callbacks.error_join = function(error)
    utils.to_console("Join event error:", error)
end

return M