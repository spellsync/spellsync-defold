local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local trigger_id = "my_trigger"

local function claim()
    spellsync.triggers.claim({ id = trigger_id }, function(result)
        utils.to_log("Claim:", result)
    end)
end

local function list()
    utils.to_log("List triggers:", spellsync.triggers.list())
end

local function activated_list()
    utils.to_log("List activated triggers:", spellsync.triggers.activated_list())
end

local function get_trigger()
    utils.to_log("Get trigger:", spellsync.triggers.get_trigger(trigger_id))
end

local function is_activated()
    utils.to_log("Is activated:", spellsync.triggers.is_activated(trigger_id))
end

local function is_claimed()
    utils.to_log("Is claimed:", spellsync.triggers.is_claimed(trigger_id))
end

local M = {
    { name = "Claim", callback = claim },
    { name = "List", callback = list },
    { name = "Activated list", callback = activated_list },
    { name = "Get trigger", callback = get_trigger },
    { name = "Is activated", callback = is_activated },
    { name = "Is claimed", callback = is_claimed },
}

spellsync.triggers.callbacks.activate = function(trigger)
    utils.to_console("Trigger activate:", trigger)
end
spellsync.triggers.callbacks.claim = function(trigger)
    utils.to_console("Trigger claim:", trigger)
end
spellsync.triggers.callbacks.error_claim = function(error)
    utils.to_console("Trigger claim error:", error)
end

return M