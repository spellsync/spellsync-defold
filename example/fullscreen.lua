local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local function open()
    utils.to_log("Fullscreen open")
    spellsync.fullscreen.open()
end

local function close()
    utils.to_log("Fullscreen close")
    spellsync.fullscreen.close()
end

local function toggle()
    utils.to_log("Fullscreen toggle")
    spellsync.fullscreen.toggle()
end

local M = {
    { name = "Open", callback = open },
    { name = "Close", callback = close },
    { name = "Toggle", callback = toggle },
}

spellsync.fullscreen.callbacks.open = function()
    utils.to_console("Fullscreen open")
end
spellsync.fullscreen.callbacks.close = function()
    utils.to_console("Fullscreen close")
end
spellsync.fullscreen.callbacks.change = function()
    utils.to_console("Fullscreen change")
end

return M