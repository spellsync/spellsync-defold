local spellsync = require("spellsync.spellsync")

local function info()
    spellsync.logger.info("Information", "count", 100)
end

local function warn()
    spellsync.logger.warn("Warning", "count", 100)
end

local function err()
    spellsync.logger.error("Error", "count", 100)
end

local function log()
    spellsync.logger.log("Log", "count", 100)
end

local M = {
    { name = "Information", callback = info },
    { name = "Warning", callback = warn },
    { name = "Error", callback = err },
    { name = "Log", callback = log }
}

return M