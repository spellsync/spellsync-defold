local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local function open()
    spellsync.games_collections.open({ tag = "ALL" }, function()
        utils.to_log("Games collections open")
    end)
end

local function fetch()
    spellsync.games_collections.fetch({ tag = "ALL" }, function(result)
        utils.to_log("Games collections fetch:", result)
    end)
end

local M = {
    { name = "Open", callback = open },
    { name = "Fetch", callback = fetch },
}

spellsync.games_collections.callbacks.open = function()
    utils.to_console("Games collections open")
end
spellsync.games_collections.callbacks.close = function()
    utils.to_console("Games collections close")
end
spellsync.games_collections.callbacks.fetch = function(result)
    utils.to_console("Games collections fetch", result)
end
spellsync.games_collections.callbacks.error_fetch = function(error)
    utils.to_console("Games collections fetch error:", error)
end

return M