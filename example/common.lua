local spellsync = require("spellsync.spellsync")
local utils = require("example.utils")

local function init()
    local is_ok, message = pcall(spellsync.init, function(success)
        utils.to_log(success)
    end)
    if not is_ok then
        utils.to_console(message)
    end
end

local function get_language()
    utils.to_log("Current language: " .. spellsync.language())
end

local function set_language_ru()
    spellsync.change_language(spellsync.languages.RUSSIAN)
    utils.to_log("Set language `ru`")
end

local function set_language_en()
    spellsync.change_language(spellsync.languages.ENGLISH)
    utils.to_log("Set language `en`")
end

local function is_mobile()
    utils.to_log("Is mobile: " .. tostring(spellsync.is_mobile()))
end

local function is_portrait()
    utils.to_log("Is portrait: " .. tostring(spellsync.is_portrait()))
end

local function is_dev()
    utils.to_log("Is dev: " .. tostring(spellsync.is_dev()))
end

local function is_allowed_origin()
    utils.to_log("Is allowed origin: " .. tostring(spellsync.is_allowed_origin()))
end

local function get_server_time()
    utils.to_log("Get server time: " .. spellsync.get_server_time())
end

local function is_paused()
    utils.to_log("Is paused: " .. tostring(spellsync.is_paused()))
end

local function pause()
    spellsync.pause()
end

local function resume()
    spellsync.resume()
end

local function set_background()
    spellsync.set_background({ url = "/image.png" })
end

local function game_start()
    spellsync.game_start()
end

local function is_game_started()
    utils.to_log("Is game started: " .. tostring(spellsync.is_game_started()))
end

local function gameplay_start()
    spellsync.gameplay_start()
end

local function gameplay_stop()
    spellsync.gameplay_stop()
end

local function is_game_play()
    utils.to_log("Is gameplay: " .. tostring(spellsync.is_game_play()))
end

local M = {
    { name = "Init", callback = init },
    { name = "Get language", callback = get_language },
    { name = "Set language ru", callback = set_language_ru },
    { name = "Set language en", callback = set_language_en },
    { name = "Is mobile?", callback = is_mobile },
    { name = "Is portrait?", callback = is_portrait },
    { name = "Is dev?", callback = is_dev },
    { name = "Is allowed origin?", callback = is_allowed_origin },
    { name = "Get server time", callback = get_server_time },
    { name = "Is paused?", callback = is_paused },
    { name = "Pause", callback = pause },
    { name = "Resume", callback = resume },
    { name = "Set background", callback = set_background },
    { name = "Game start", callback = game_start },
    { name = "Is game started", callback = is_game_started },
    { name = "Gameplay start", callback = gameplay_start },
    { name = "Gameplay stop", callback = gameplay_stop },
    { name = "Is gameplay", callback = is_game_play }
}

spellsync.callbacks.change_orientation = function(portrait)
    utils.to_console("Change orientation, is_portrait?:", portrait)
end
spellsync.callbacks.pause = function()
    utils.to_console("pause")
end
spellsync.callbacks.resume = function()
    utils.to_console("resume")
end
spellsync.callbacks.event_connect = function()
    utils.to_console("notification of other windows")
end

return M