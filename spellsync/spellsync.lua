local M = {}

local version = string.format("SpellSync for Defold v1.1.5")
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

M.callbacks = callbacks.common

M.languages = {
    ENGLISH = "en",
    RUSSIAN = "ru",
    FRENCH = "fr",
    ITALIAN = "it",
    GERMAN = "de",
    SPANISH = "es",
    CHINESE = "zh",
    PORTUGUESE = "pt",
    KOREAN = "ko",
    JAPANESE = "ja",
    TURKISH = "tr",
    ARAB = "ar",
    HINDI = "hi",
    INDONESIAN = "id"
}

--- Initialize SpellSync
---@param callback function callback function to be called upon initialization completion: callback(success)
function M.init(callback)
    core.init(callback)
end

--- Get the current language
---@return string language code in ISO 639-1 format
function M.language()
    return core.call_api("language")
end

--- Set the language
---@param language_code string language code in ISO 639-1 format
function M.change_language(language_code)
    if type(language_code) ~= "string" then
        error("The language code must be a string!", 2)
    end
    core.call_api("changeLanguage", language_code)
end

--- Is it a mobile device?
---@return boolean
function M.is_mobile()
    return core.call_api("isMobile") == true
end

--- Screen mode: portrait/landscape?
---@return boolean
function M.is_portrait()
    return core.call_api("isPortrait") == true
end

--- In development mode?
---@return boolean
function M.is_dev()
    return core.call_api("isDev") == true
end

--- Is the game host in trusted sources?
---@return boolean
function M.is_allowed_origin()
    return core.call_api("isAllowedOrigin") == true
end

--- Retrieve server time
---@return string
function M.get_server_time()
    return core.call_api("serverTime")
end

--- Is it paused?
---@return boolean
function M.is_paused()
    return core.call_api("isPaused") == true
end

--- Pause the game
function M.pause()
    core.call_api("pause")
end

--- Resume
function M.resume()
    core.call_api("resume")
end

--- Set the game background image
---@param parameters table parameters
function M.set_background(parameters)
    helpers.check_table_required(parameters)
    core.call_api("setBackground", { parameters })
end

--- Start the game
function M.game_start()
    core.call_api("gameStart")
end

--- Has the game started (loading completed)?
function M.is_game_started()
    return core.call_api("isGameStarted") == true
end

--- Start gameplay
function M.gameplay_start()
    core.call_api("gameplayStart")
end

--- Stop gameplay
function M.gameplay_stop()
    core.call_api("gameplayStop")
end

function M.is_game_play()
    return core.call_api("isGameplay") == true
end

--- Get the plugin version
---@return string
function M.get_plugin_version()
    return version
end

M.app = require("spellsync.core.app")
M.platform = require("spellsync.core.platform")
M.player = require("spellsync.core.player")
M.players = require("spellsync.core.players")
M.payments = require("spellsync.core.payments")
M.leaderboard = require("spellsync.core.leaderboard")
M.channels = require("spellsync.core.channels")
M.events = require("spellsync.core.events")
M.rewards = require("spellsync.core.rewards")
M.schedulers = require("spellsync.core.schedulers")
M.triggers = require("spellsync.core.triggers")
M.ads = require("spellsync.core.ads")
M.achievements = require("spellsync.core.achievements")
M.socials = require("spellsync.core.socials")
M.variables = require("spellsync.core.variables")
M.games_collections = require("spellsync.core.games_collections")
M.images = require("spellsync.core.images")
M.files = require("spellsync.core.files")
M.documents = require("spellsync.core.documents")
M.analytics = require("spellsync.core.analytics")
M.fullscreen = require("spellsync.core.fullscreen")
M.logger = require("spellsync.core.logger")

return M
