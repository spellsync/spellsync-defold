-- Define an empty table for the M module
local M = {}
-- Require core, helpers, and callbacks modules from spellsync.core
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

-- Define rarity levels for achievements
M.rare = {
    COMMON = "COMMON",
    UNCOMMON = "UNCOMMON",
    RARE = "RARE",
    EPIC = "EPIC",
    LEGENDARY = "LEGENDARY",
    MYTHIC = "MYTHIC"
}

--- List of achievement groups
---@return table result
function M.groups_list()
    return core.call_api("achievements.groupsList")
end

--- List of achievements
---@return table result
function M.list()
    return core.call_api("achievements.list")
end

--- List of unlocked achievements
---@return table result
function M.unlocked_list()
    return core.call_api("achievements.unlockedList")
end

--- Unlock an achievement
---@param parameters table parameters
---@param callback function callback function for unlocking achievement result: callback(result)
function M.unlock(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("achievements.unlock", { parameters }, callback)
end

--- Set progress on an achievement
---@param parameters table parameters
---@param callback function callback function for setting achievement progress result: callback(result)
function M.set_progress(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("achievements.setProgress", { parameters }, callback)
end

--- Check if a player has an achievement
---@param id_or_tag string|number tag or identifier of the achievement
---@return boolean result
function M.has(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("achievements.has", id_or_tag) == true
end

--- Get progress of an achievement
---@param id_or_tag string|number tag or identifier of the achievement
---@return number result
function M.get_progress(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("achievements.getProgress", id_or_tag)
end

--- Open achievements in an overlay
---@param callback function callback function for the result of opening achievements: callback()
function M.open(callback)
    helpers.check_callback(callback)
    core.call_api("achievements.open", nil, callback)
end

--- Fetch achievements
---@param callback function callback function for the result of fetching achievements: callback(achievements)
function M.fetch(callback)
    helpers.check_callback(callback)
    return core.call_api("achievements.fetch", nil, callback)
end

-- Assign the callbacks related to achievements
M.callbacks = callbacks.achievements

-- Return the M module
return M