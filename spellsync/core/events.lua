local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Join an event
---@param parameters table parameters
---@param callback function callback function to call upon participating in the event result: callback(result)
function M.join(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("events.join", { parameters }, callback)
end

---List of events
---@return table result
function M.list()
    return core.call_api("events.list")
end

---List of player's active events
---@return table result
function M.active_list()
    return core.call_api("events.activeList")
end

---Get information about an event
---@param id_or_tag string|number tag or identifier of the event
---@return table result
function M.get_event(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("events.getEvent", id_or_tag)
end

---Event is active
---@param id_or_tag string|number tag or identifier of the reward
---@return boolean result
function M.has(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("events.has", id_or_tag) == true
end

---Player is participating in the event
---@param id_or_tag string|number tag or identifier of the reward
---@return boolean result
function M.is_joined(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("events.isJoined", id_or_tag) == true
end

M.callbacks = callbacks.events

return M
