-- Define an empty table for the M module
local M = {}
-- Require core and helpers modules from spellsync.core
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")

--- Visit or view a page
---@param url string page address
function M.hit(url)
    helpers.check_string(url, "url")
    core.call_api("analytics.hit", { url })
end

--- Send goal achievement
---@param event string event
---@param value string|number|boolean|nil value
function M.goal(event, value)
    helpers.check_string(event, "event")
    if value then
        helpers.check_value(value)
    end
    core.call_api("analytics.goal", { event, value })
end

return M
