local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Upload a file
---@param parameters table
---@param callback function callback function to call upon file upload result: callback(result)
function M.upload(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("files.upload", { parameters }, callback)
end

---Upload file from URL
---@param parameters table
---@param callback function callback function to call upon file upload from URL result: callback(result)
function M.upload_url(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("files.uploadUrl", { parameters }, callback)
end

---Upload content
---@param parameters table
---@param callback function callback function to call upon content upload result: callback(result)
function M.upload_content(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("files.uploadContent", { parameters }, callback)
end

---Get content
---@param uri string filename to load
---@param callback function callback function to call upon getting the file content result: callback(text)
function M.load_content(uri, callback)
    helpers.check_string(uri, "uri")
    helpers.check_callback(callback)
    core.call_api("files.loadContent", { uri }, callback)
end

---Choose a file
---@param accept string file types to choose from
---@param callback function callback function to call upon file selection result: callback(result)
function M.choose_file(accept, callback)
    helpers.check_string(accept, "accept", true)
    helpers.check_callback(callback)
    core.call_api("files.chooseFile", nil, callback)
end

---Get files
---@param parameters table
---@param callback function callback function to call upon getting files result: callback(result)
function M.fetch(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("files.fetch", { parameters }, callback)
end

---Get more files
---@param parameters table
---@param callback function callback function to call upon getting more files result: callback(result)
function M.fetch_more(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("files.fetchMore", { parameters }, callback)
end

---Check if file uploads are allowed
---@return boolean possibility of uploading files
function M.can_upload()
    return core.call_api("files.canUpload") == true
end

M.callbacks = callbacks.files

return M
