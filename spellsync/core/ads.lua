-- Define an empty table for the M module
local M = {}
-- Require core, helpers, and callbacks modules from spellsync.core
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

--- Check if AdBlock is enabled
---@return boolean
function M.is_adblock_enabled()
    return core.call_api("ads.isAdblockEnabled") == true
end

--- Check banner availability
---@return boolean
function M.is_sticky_available()
    return core.call_api("ads.isStickyAvailable") == true
end

--- Check fullscreen ad availability
---@return boolean
function M.is_fullscreen_available()
    return core.call_api("ads.isFullscreenAvailable") == true
end

--- Check rewarded ad availability
---@return boolean
function M.is_rewarded_available()
    return core.call_api("ads.isRewardedAvailable") == true
end

--- Check preload ad availability
---@return boolean
function M.is_preloader_available()
    return core.call_api("ads.isPreloaderAvailable") == true
end

--- Check if banner is playing
---@return boolean
function M.is_sticky_playing()
    return core.call_api("ads.isStickyPlaying") == true
end

--- Check if fullscreen ad is playing
---@return boolean
function M.is_fullscreen_playing()
    return core.call_api("ads.isFullscreenPlaying") == true
end

--- Check if rewarded ad is playing
---@return boolean
function M.is_rewarded_playing()
    return core.call_api("ads.isRewardedPlaying") == true
end

--- Check if preload ad is playing
---@return boolean
function M.is_preloader_playing()
    return core.call_api("ads.isPreloaderPlaying") == true
end

--- Check if countdown overlay is enabled before showing fullscreen ad
---@return boolean
function M.is_countdown_overlay_enabled()
    return core.call_api("ads.isCountdownOverlayEnabled") == true
end

--- Check if overlay is enabled when a rewarded video fails to play
---@return boolean
function M.is_rewarded_failed_overlay_enabled()
    return core.call_api("ads.isRewardedFailedOverlayEnabled") == true
end

--- Check if fullscreen ad can be shown before gameplay on the platform
---@return boolean
function M.can_show_fullscreen_before_gameplay()
    return core.call_api("ads.canShowFullscreenBeforeGamePlay") == true
end

--- Show fullscreen ad
---@param callback function callback function for ad completion: callback(result)
---@param parameters table parameters
function M.show_fullscreen(callback, parameters)
    helpers.check_callback(callback)
    helpers.check_table(parameters)
    core.call_api("ads.showFullscreen", { parameters }, callback)
end

--- Show banner ad (preloader)
---@param callback function callback function for ad completion: callback(result)
function M.show_preloader(callback)
    helpers.check_callback(callback)
    core.call_api("ads.showPreloader", nil, callback)
end

--- Show rewarded ad
---@param callback function callback function for ad completion: callback(result)
---@param parameters table parameters
function M.show_rewarded_video(callback, parameters)
    helpers.check_callback(callback)
    helpers.check_table(parameters)
    core.call_api("ads.showRewardedVideo", { parameters }, callback)
end

--- Show banner
---@param callback function callback function for result of showing: callback(result)
function M.show_sticky(callback)
    helpers.check_callback(callback)
    core.call_api("ads.showSticky", nil, callback)
end

--- Refresh banner
---@param callback function callback function for refresh result: callback(result)
function M.refresh_sticky(callback)
    helpers.check_callback(callback)
    core.call_api("ads.refreshSticky", nil, callback)
end

--- Close banner
---@param callback function callback function for close result: callback()
function M.close_sticky(callback)
    helpers.check_callback(callback)
    core.call_api("ads.closeSticky", nil, callback)
end

-- Assign the callbacks related to ads
M.callbacks = callbacks.ads

-- Return the M module
return M