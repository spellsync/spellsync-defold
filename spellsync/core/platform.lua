local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")

M.NONE = "NONE"
M.CRAZY_GAMES = "CRAZY_GAMES"
M.GAME_DISTRIBUTION = "GAME_DISTRIBUTION"
M.GAME_MONETIZE = "GAME_MONETIZE"
M.OK = "OK"
M.SMARTMARKET = "SMARTMARKET"
M.VK = "VK"
M.YANDEX = "YANDEX"
M.GAMEPIX = "GAMEPIX"
M.POKI = "POKI"
M.VK_PLAY = "VK_PLAY"
M.WG_PLAYGROUND = "WG_PLAYGROUND"
M.KONGREGATE = "KONGREGATE"
M.CUSTOM = "CUSTOM"

---Platform Type
---@return string
function M.type()
    return core.call_api("platform.type")
end

---Integrated Authentication Capability
---@return boolean
function M.has_integrated_auth()
    return core.call_api("platform.hasIntegratedAuth") == true
end

---Ability to Place External Links
---@return boolean
function M.is_external_links_allowed()
    return core.call_api("platform.isExternalLinksAllowed") == true
end

---Secret Code Authorization Available
---@return string
function M.is_secret_code_auth_available()
    return core.call_api("platform.isSecretCodeAuthAvailable") == true
end

---Execute a Native Platform Method. If the method is an object API.
---@param method string the method to be executed, object method or object field, for example: "feedback.canReview"
---@param parameters any method parameters, if there are several parameters, then a table (array) of parameters.
---@param callback function|nil callback function
---@return any operation result or return data
function M.call_native_sdk(method, parameters, callback)
    if type(method) ~= "string" or method == "" then
        error("The method must be a string!", 2)
    end
    helpers.check_callback(callback)
    return core.call_api(method, parameters, callback, true)
end

return M
