local M = {}
local core = require("spellsync.core.core")

---Number of days in the game
---@return boolean
function M.active_days()
    return core.call_api("player.stats.activeDays")
end

---Number of consecutive days in the game
---@return boolean
function M.active_days_consecutive()
    return core.call_api("player.stats.activeDaysConsecutive")
end

---Number of seconds spent in the game today
---@return boolean
function M.playtime_today()
    return core.call_api("player.stats.playtimeToday")
end

---Number of seconds spent in the game overall
---@return boolean
function M.playtime_all()
    return core.call_api("player.stats.playtimeAll")
end

return M
