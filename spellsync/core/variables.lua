local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

M.types = {
    DATA = "data",
    STATS = "stats",
    FLAG = "flag",
    DOC_HTML = "doc_html",
    IMAGE = "image",
    FILE = "file"
}

---Request variables
---@param callback function callback function to handle the variable request result: callback()
function M.fetch(callback)
    helpers.check_callback(callback)
    core.call_api("variables.fetch", nil, callback)
end

---Get the value of a variable
---@param variable string variable name
function M.get(variable)
    helpers.check_string(variable, "variable")
    return core.call_api("variables.get", { variable })
end

---Check the existence of a variable
---@param variable string variable name
---@return boolean result
function M.has(variable)
    helpers.check_string(variable, "variable")
    return core.call_api("variables.has", { variable }) == true
end

---Get the type of a variable
---@param variable string variable name
---@return string result
function M.type(variable)
    helpers.check_string(variable, "variable")
    return core.call_api("variables.type", { variable })
end

M.callbacks = callbacks.variables

return M
