-- Define an empty table for the M module
local M = {}
-- Require core and helpers modules from spellsync.core
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")

---Get the game title
---@return string
function M.title()
    return core.call_api("app.title")
end

---Get the game description
---@return string
function M.description()
    return core.call_api("app.description")
end

---Get the game image
---@return string
function M.image()
    return core.call_api("app.image")
end

---Get the game URL
---@return string
function M.url()
    return core.call_api("app.url")
end

---Prompt the player to leave a review for the game
---@param callback function callback function: callback(result)
function M.request_review(callback)
    helpers.check_callback(callback)
    core.call_api("app.requestReview", nil, callback)
end

---Check if the ability to leave a review for the game is supported
---@return boolean
function M.can_request_review()
    return core.call_api("app.canRequestReview") == true
end

---Check if a review has already been left
---@return boolean
function M.is_already_reviewed()
    return core.call_api("app.isAlreadyReviewed") == true
end

---Prompt the player to create a shortcut or add the game to favorites
---@return boolean
function M.add_shortcut(callback)
    helpers.check_callback(callback)
    core.call_api("app.addShortcut", nil, callback)
end

---Check if the ability to create a shortcut or add the game to favorites is supported
---@return boolean
function M.can_add_shortcut()
    return core.call_api("app.canAddShortcut") == true
end

return M
