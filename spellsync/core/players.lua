local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")

---Fetch player data
---@param parameters table parameters
---@param callback function callback function for getting data of other players: callback(result)
function M.fetch(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("players.fetch", { parameters }, callback)
end

return M
