local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")

---Show leaderboard in-game overlay
---@param parameters table|nil display parameters
function M.open(parameters)
    helpers.check_table(parameters)
    core.call_api("leaderboard.open", { parameters })
end

---Get leaderboard
---@param parameters table|nil display parameters
---@param callback function callback function for handling the result of fetching the leaderboard: callback(leaders)
function M.fetch(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("leaderboard.fetch", { parameters }, callback)
end

---Get player rating
---@param parameters table|nil display parameters
---@param callback function callback function for handling the result of fetching the leaderboard: callback(leaders)
function M.fetch_player_rating(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("leaderboard.fetchPlayerRating", { parameters }, callback)
end

---Show isolated leaderboard in-game overlay
---@param parameters table|nil display parameters
---@param callback function callback function for handling the result of showing: callback(result)
function M.open_scoped(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("leaderboard.openScoped", { parameters }, callback)
end

---Get isolated leaderboard
---@param parameters table|nil display parameters
---@param callback function callback function for handling the result of fetching the leaderboard: callback(leaders)
function M.fetch_scoped(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("leaderboard.fetchScoped", { parameters }, callback)
end

---Publish player's record to isolated leaderboard
---@param parameters table table with parameters and record to be written
---@param callback function callback function for handling the result of record publication: callback(result)
function M.publish_record(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("leaderboard.publishRecord", { parameters }, callback)
end

---Get player rating in isolated leaderboard
---@param parameters table display parameters
---@param callback function callback function for handling the result of fetching the leaderboard: callback(leaders)
function M.fetch_player_rating_scoped(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("leaderboard.fetchPlayerRatingScoped", { parameters }, callback)
end

return M
