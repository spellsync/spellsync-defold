local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Get reward for a trigger
---@param parameters table parameters
---@param callback function callback function to handle the reward issuance result: callback(result)
function M.claim(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("triggers.claim", { parameters }, callback)
end

---List of triggers
---@return table result
function M.list()
    return core.call_api("triggers.list")
end

---List of activated triggers
---@return table result
function M.activated_list()
    return core.call_api("triggers.activatedList")
end

---Get information about a reward
---@param id_or_tag string tag or identifier of the reward
---@return table result
function M.get_trigger(id_or_tag)
    helpers.check_string(id_or_tag, "id_or_tag")
    return core.call_api("triggers.getTrigger", id_or_tag)
end

---Check if a trigger is activated
---@param id_or_tag string tag or identifier of the reward
---@return boolean result
function M.is_activated(id_or_tag)
    helpers.check_string(id_or_tag, "id_or_tag")
    return core.call_api("triggers.isActivated", id_or_tag) == true
end

---Check if a reward is claimed
---@param id_or_tag string tag or identifier of the reward
---@return boolean result
function M.is_claimed(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("triggers.isClaimed", id_or_tag) == true
end

M.callbacks = callbacks.triggers

return M
