local M = {}
local core = require("spellsync.core.core")
local callbacks = require("spellsync.core.callbacks")

---Enter full-screen mode
function M.open()
    core.call_api("fullscreen.open")
end

---Exit full-screen mode
function M.close()
    core.call_api("fullscreen.close")
end

---Toggle full-screen mode
function M.toggle()
    core.call_api("fullscreen.toggle")
end

M.callbacks = callbacks.fullscreen

return M
