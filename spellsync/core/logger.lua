local M = {}
local core = require("spellsync.core.core")

---Toast with information
---@param ... any information to display
function M.info(...)
    core.call_api("logger.info", { ... })
end

---Toast with warning
---@param ... any information to display
function M.warn(...)
    core.call_api("logger.warn", { ... })
end

---Toast with error
---@param ... any information to display
function M.error(...)
    core.call_api("logger.error", { ... })
end

---Log to console
---@param ... any information to display
function M.log(...)
    core.call_api("logger.log", { ... })
end

return M
