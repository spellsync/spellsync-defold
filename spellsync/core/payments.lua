local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Check payment support on the platform
---@return boolean result
function M.is_available()
    return core.call_api("payments.isAvailable") == true
end

---List of products
---@return table result
function M.products()
    return core.call_api("payments.products")
end

---List of purchases
---@return table result
function M.purchases()
    return core.call_api("payments.purchases")
end

---Purchase
---@param parameters table parameters
---@param callback function callback function for purchase result: callback(result)
function M.purchase(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("payments.purchase", { parameters }, callback)
end

---Use purchase
---@param parameters table parameters
---@param callback function callback function for product usage result: callback(result)
function M.consume(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("payments.consume", { parameters }, callback)
end

---Check purchase availability
---@param product string|number product tag or identifier
---@return boolean result
function M.has(product)
    helpers.check_string_or_number(product, "product")
    return core.call_api("payments.has", product) == true
end

---Get list of products
---@param callback function callback function for retrieving list of products result: callback(result)
function M.fetch_products(callback)
    helpers.check_callback(callback)
    core.call_api("payments.fetchProducts", nil, callback)
end

---Check subscription support on the platform
---@return boolean result
function M.is_subscriptions_available()
    return core.call_api("payments.isSubscriptionsAvailable") == true
end

---Subscribe
---@param parameters table parameters
---@param callback function callback function for subscription result: callback(result)
function M.subscribe(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("payments.subscribe", { parameters }, callback)
end

---Unsubscribe
---@param parameters table parameters
---@param callback function callback function for subscription cancellation result: callback(result)
function M.unsubscribe(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("payments.unsubscribe", { parameters }, callback)
end

M.callbacks = callbacks.payments

return M
