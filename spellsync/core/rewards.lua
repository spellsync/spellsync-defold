local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Give reward
---@param parameters table parameters
---@param callback function callback function for giving reward result: callback(result)
function M.give(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("rewards.give", { parameters }, callback)
end

---Accept reward
---@param parameters table parameters
---@param callback function callback function for accepting reward result: callback(result)
function M.accept(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("rewards.accept", { parameters }, callback)
end

---List rewards
---@return table result
function M.list()
    return core.call_api("rewards.list")
end

---List given rewards
---@return table result
function M.given_list()
    return core.call_api("rewards.givenList")
end

---Get reward information
---@param id_or_tag string|number tag or identifier of the reward
---@return table result
function M.get_reward(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("rewards.getReward", id_or_tag)
end

---Check if reward is given
---@param id_or_tag string|number tag or identifier of the reward
---@return boolean result
function M.has(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("rewards.has", id_or_tag) == true
end

---Check if reward is accepted
---@param id_or_tag string|number tag or identifier of the reward
---@return boolean result
function M.has_accepted(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("rewards.hasAccepted", id_or_tag) == true
end

---Reward is given but not accepted
---@param id_or_tag string|number tag or identifier of the reward
---@return boolean result
function M.has_unaccepted(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("rewards.hasUnaccepted", id_or_tag) == true
end

M.callbacks = callbacks.rewards

return M
