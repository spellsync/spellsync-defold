local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Player is logged in
---@return boolean
function M.is_logged_in()
    return core.call_api("player.isLoggedIn") == true
end

---Player has any credentials for login (cookie, authorization, secret code)
---@return boolean
function M.has_any_credentials()
    return core.call_api("player.hasAnyCredentials") == true
end

---Player is logged in on the platform
---@return boolean
function M.is_logged_in_by_platform()
    return core.call_api("player.isLoggedInByPlatform") == true
end

---Player synchronization
---@param parameters table parameters
---@param callback function callback function for synchronization result: callback()
function M.sync(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("player.sync", { parameters }, callback)
end

---Load player from server with overwriting local data
---@param callback function callback function for synchronization result: callback()
function M.load(callback)
    helpers.check_callback(callback)
    core.call_api("player.load", nil, callback)
end

---Login
---@param callback function callback function for login result: callback(result)
function M.login(callback)
    helpers.check_callback(callback)
    core.call_api("player.login", nil, callback)
end

---Get player's fields list
---@param callback function callback function for getting player's fields result: callback()
function M.fetch_fields(callback)
    helpers.check_callback(callback)
    core.call_api("player.fetchFields", nil, callback)
end

---Player ID
---@return number
function M.id()
    return core.call_api("player.id")
end

---Player score
---@return number
function M.score()
    return core.call_api("player.score") or 0
end

---Player name
---@return string
function M.name()
    return core.call_api("player.name") or ""
end

---Player avatar link
---@return string
function M.avatar()
    return core.call_api("player.avatar")
end

---Stub check - whether the player is empty or the data in it differs from default
---@return boolean
function M.is_stub()
    return core.call_api("player.isStub") == true
end

---Player fields
---@return table
function M.fields()
    return core.call_api("player.fields")
end

---Get the value of the field key
---@param key string key
---@return string|number|boolean value of the key field
function M.get(key)
    helpers.check_key(key)
    return core.call_api("player.get", key)
end

---Set the value of the field key
---@param key string key
---@param value string|number|boolean value of the key field
function M.set(key, value)
    helpers.check_key(key)
    helpers.check_value(value)
    core.call_api("player.set", { key, value })
end

---Add value to the field key
---@param key string key
---@param value number value to be added
function M.add(key, value)
    helpers.check_key(key)
    helpers.check_number(value, "value")
    core.call_api("player.add", { key, value })
end

---Toggle the value of the field key
---@param key string key
function M.toggle(key)
    helpers.check_key(key)
    core.call_api("player.toggle", key)
end

---Check if the field key exists and it's not empty (not 0, '', false, null, undefined)
---@param key string key
---@return boolean result
function M.has(key)
    helpers.check_key(key)
    return core.call_api("player.has", key) == true
end

---Return player state as an object
---@return table table containing player data
function M.to_json()
    return core.call_api("player.toJSON")
end

---Set player state from an object
---@param parameters table table containing player data
function M.from_json(parameters)
    helpers.check_table(parameters)
    core.call_api("player.fromJSON", { parameters })
end

---Reset player state to default
function M.reset()
    core.call_api("player.reset")
end

---Remove player - reset fields and clear ID
function M.remove()
    core.call_api("player.remove")
end

---Get field by key
---@param key string key
---@return table result
function M.get_field(key)
    helpers.check_key(key)
    return core.call_api("player.getField", key)
end

---Get translated field name by key
---@param key string key
---@return string result
function M.get_field_name(key)
    helpers.check_key(key)
    return core.call_api("player.getFieldName", key)
end

---Get translated field variant name (enum) by key and its value
---@param key string key
---@param value string value
---@return table result
function M.get_field_variant_name(key, value)
    helpers.check_key(key)
    if type(value) ~= "string" then
        error("The value must be a string!", 2)
    end
    return core.call_api("player.getFieldVariantName", { key, value })
end

M.stats = require("spellsync.core.player_stats")
M.callbacks = callbacks.player

return M
