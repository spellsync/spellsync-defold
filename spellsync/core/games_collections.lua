local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Open overlay with games
---@param parameters table parameters
---@param callback function callback function for handling the result of opening the overlay: callback()
function M.open(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("gamesCollections.open", { parameters }, callback)
end

---Get game collection
---@param parameters table parameters
---@param callback function callback function for handling the result of fetching the game collection: callback(result)
---@return table
function M.fetch(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("gamesCollections.fetch", { parameters }, callback)
end

M.callbacks = callbacks.games_collections

return M
