local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Open privacy policy
---@param parameters table
function M.open(parameters)
    helpers.check_table_required(parameters)
    core.call_api("documents.open", { parameters })
end

---Get privacy policy
---@param parameters table
---@param callback function callback function to call upon retrieving the privacy policy result: callback(document)
function M.fetch(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("documents.fetch", { parameters }, callback)
end

M.callbacks = callbacks.documents

return M
