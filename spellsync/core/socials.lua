local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")

---Check if sharing is supported
---@return boolean result
function M.is_supports_share()
    return core.call_api("socials.isSupportsShare") == true
end

---Check support for native sharing
---@return boolean result
function M.is_supports_native_share()
    return core.call_api("socials.isSupportsNativeShare") == true
end

---Share
---@param parameters table|nil
function M.share(parameters)
    helpers.check_table(parameters)
    core.call_api("socials.share", { parameters })
end

---Check if native posting is supported
---@return boolean result
function M.is_supports_native_posts()
    return core.call_api("socials.isSupportsNativePosts") == true
end

---Post
---@param parameters table|nil
function M.post(parameters)
    helpers.check_table(parameters)
    core.call_api("socials.post", { parameters })
end

---Check support for native invites
---@return boolean result
function M.is_supports_native_invite()
    return core.call_api("socials.isSupportsNativeInvite") == true
end

---Invite friends
---@param parameters table|nil
function M.invite(parameters)
    helpers.check_table(parameters)
    core.call_api("socials.invite", { parameters })
end

---Can invite to the community on the current platform
---@return boolean result
function M.can_join_community()
    return core.call_api("socials.canJoinCommunity") == true
end

---Check if native community join is supported
---@return boolean result
function M.is_supports_native_community_join()
    return core.call_api("socials.isSupportsNativeCommunityJoin") == true
end

---Join a community
function M.join_community()
    core.call_api("socials.joinCommunity")
end

---Create a sharing URL
---@param parameters table|nil
---@return string result
function M.make_share_url(parameters)
    helpers.check_table_required(parameters)
    return core.call_api("socials.makeShareUrl", { parameters })
end

---Read parameters when following a link
---@param parameter string parameter
---@return string result
function M.get_share_param(parameter)
    helpers.check_string(parameter, "parameter")
    return core.call_api("socials.getShareParam", { parameter })
end

return M
