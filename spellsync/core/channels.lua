-- Define an empty table for the M module
local M = {}
-- Require core, helpers, and callbacks modules from spellsync.core
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Create a channel
---@param parameters table parameters
---@param callback function callback function: callback(channel)
function M.create_channel(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.createChannel", { parameters }, callback)
end

---Update a channel
---@param parameters table parameters
---@param callback function callback function: callback(channel)
function M.update_channel(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.updateChannel", { parameters }, callback)
end

---Delete a channel
---@param parameters table parameters
---@param callback function callback function: callback()
function M.delete_channel(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.deleteChannel", { parameters }, callback)
end

---Fetch channel information by ID
---@param parameters table parameters
---@param callback function callback function: callback(channel)
function M.fetch_channel(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.fetchChannel", { parameters }, callback)
end

---Fetch a list of channels
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.fetch_channels(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.fetchChannels", { parameters }, callback)
end

---Fetch more channels
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.fetch_more_channels(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.fetchMoreChannels", { parameters }, callback)
end

---Open chat window
---@param parameters table parameters
function M.open_chat(parameters)
    helpers.check_table(parameters)
    core.call_api("channels.openChat", { parameters })
end

---Check if main chat is enabled
---@return boolean result
function M.is_main_chat_enabled()
    return core.call_api("channels.isMainChatEnabled") == true
end

---Get the ID of the main chat
---@return number result
function M.main_chat_id()
    return core.call_api("channels.mainChatId")
end

---Request to join a public channel
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.join(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.join", { parameters }, callback)
end

---Cancel a join request
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.cancel_join(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.cancelJoin", { parameters }, callback)
end

---Leave a channel
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.leave(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.leave", { parameters }, callback)
end

---Kick a member from a channel
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.kick(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.kick", { parameters }, callback)
end

---Fetch members of a channel
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.fetch_members(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.fetchMembers", { parameters }, callback)
end

---Fetch more members of a channel
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.fetch_more_members(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.fetchMoreMembers", { parameters }, callback)
end

---Mute a player in a channel
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.mute(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.mute", { parameters }, callback)
end

---Unmute a player
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.unmute(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.unmute", { parameters }, callback)
end

---Send an invitation
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.send_invite(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.sendInvite", { parameters }, callback)
end

---Cancel an invitation
---@param parameters table parameters
---@param callback function callback function: callback(result)
function M.cancel_invite(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("channels.cancelInvite", { parameters }, callback)
end

-- (Truncated for brevity)

M.callbacks = callbacks.channels

return M