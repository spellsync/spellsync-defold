local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Claim reward for the day
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@param callback function callback function for giving reward result: callback(result)
function M.claim_day(id_or_tag, day, callback)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    helpers.check_callback(callback)
    core.call_api("schedulers.claimDay", { id_or_tag, day }, callback)
end

---Claim additional activity reward for the day
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@param trigger_id_or_tag string tag or identifier of the trigger
---@param callback function callback function for giving reward result: callback(result)
function M.claim_day_additional(id_or_tag, day, trigger_id_or_tag, callback)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    helpers.check_string(trigger_id_or_tag, "trigger_id_or_tag")
    helpers.check_callback(callback)
    core.call_api("schedulers.claimDayAdditional", { id_or_tag, day, trigger_id_or_tag }, callback)
end

---Claim all activities reward for the day
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@param callback function callback function for giving reward result: callback(result)
function M.claim_all_day(id_or_tag, day, callback)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    helpers.check_callback(callback)
    core.call_api("schedulers.claimAllDay", { id_or_tag, day }, callback)
end

---Claim all completed days rewards
---@param id_or_tag string|number tag or identifier of the scheduler
---@param callback function callback function for giving reward result: callback(result)
function M.claim_all_days(id_or_tag, callback)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_callback(callback)
    core.call_api("schedulers.claimAllDays", id_or_tag, callback)
end

---List schedulers
---@return table result
function M.list()
    return core.call_api("schedulers.list")
end

---List active player's schedulers
---@return table result
function M.active_list()
    return core.call_api("schedulers.activeList")
end

---Get scheduler information
---@param id_or_tag string|number tag or identifier of the scheduler
---@return table result
function M.get_scheduler(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("schedulers.getScheduler", id_or_tag)
end

---Get scheduler day information
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@return table result
function M.get_scheduler_day(id_or_tag, day)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    return core.call_api("schedulers.getSchedulerDay", { id_or_tag, day })
end

---Get current day information of the scheduler
---@param id_or_tag string|number tag or identifier of the scheduler
---@return table result
function M.get_scheduler_current_day(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("schedulers.getSchedulerCurrentDay", id_or_tag)
end

---Player is registered in the scheduler
---@param id_or_tag string|number tag or identifier of the scheduler
---@return table result
function M.is_registered(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("schedulers.isRegistered", id_or_tag) == true
end

---Reward is claimed today
---@param id_or_tag string|number tag or identifier of the scheduler
---@return table result
function M.is_today_reward_claimed(id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    return core.call_api("schedulers.isTodayRewardClaimed", id_or_tag) == true
end

---Reward for the day can be claimed
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@return table result
function M.can_claim_day(id_or_tag, day)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    return core.call_api("schedulers.canClaimDay", { id_or_tag, day }) == true
end

---Additional activity reward for the day can be claimed
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@param trigger_id_or_tag string tag or identifier of the trigger
---@return table result
function M.can_claim_day_additional(id_or_tag, day, trigger_id_or_tag)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    helpers.check_string(trigger_id_or_tag, "trigger_id_or_tag")
    return core.call_api("schedulers.canClaimDayAdditional", { id_or_tag, day, trigger_id_or_tag }) == true
end

---All activities reward for the day can be claimed
---@param id_or_tag string|number tag or identifier of the scheduler
---@param day number day
---@return table result
function M.can_claim_all_day(id_or_tag, day)
    helpers.check_string_or_number(id_or_tag, "id_or_tag")
    helpers.check_number(day, "day")
    return core.call_api("schedulers.canClaimAllDay", { id_or_tag, day }) == true
end

M.callbacks = callbacks.schedulers

return M
