local M = {}

local helpers = require("spellsync.core.helpers")
local callback_ids = require("spellsync.core.callback_ids")
local callbacks = require("spellsync.core.callbacks")

if not html5 then
    spellsync = require("spellsync.mock")
end

local is_init = false

local function get_event_callback_name(callback_id)
    for callback_group, list_callbacks in pairs(callback_ids) do
        for callback_name, id in pairs(list_callbacks) do
            if callback_id == id then
                callback_name = string.gsub(string.gsub(callback_name, ":", "_"), "%u", function(c)
                    return string.format("_%s", string.lower(c))
                end)
                return callback_group, callback_name
            end
        end
    end
end

local function find_callback(callback_group, callback_name)
    for group, section_callbacks in pairs(callbacks) do
        if group == callback_group then
            return section_callbacks[callback_name]
        end
    end
end

local function decode_result(result)
    if result then
        local is_ok, data = pcall(json.decode, result)
        if is_ok then
            if data and type(data) == "table" then
                if data.error and data.error ~= "" then
                    if type(data.error) == "table" then
                        pprint("Error:", data.error)
                    else
                        print(string.format("Error: %s", data.error))
                    end
                end
                if data.object then
                    local is_object_ok, object = pcall(json.decode, data.object)
                    if is_object_ok then
                        return object
                    else
                        return data.object
                    end
                end
                return data.value
            end
            return data
        else
            return result
        end
    end
end

local function on_event_callback(self, message, id)
    local callback_group, callback_name = get_event_callback_name(id)
    if callback_name ~= nil then
        local callback = find_callback(callback_group, callback_name)
        if callback == nil then
            return
        end
        assert(type(callback) == "function", string.format("callbacks.'%s' must be a function!", callback_name))
        if message == "" then
            callback()
        else
            callback(decode_result(message))
        end
    end
end

---Initialize SpellSync
---@param callback function callback function to call upon initialization completion: callback(success)
function M.init(callback)
    if is_init then
        error("SpellSync already initialized!", 2)
    end
    if callback == nil then
        error("Callback function must be specified!", 2)
    end
    helpers.check_callback(callback)
    if sys.get_config_number("spellsync.id", -1) == -1 then
        error("SpellSync game id not set!")
    end
    if sys.get_config_string("spellsync.token", nil) == nil then
        error("SpellSync game token not set!")
    end
    is_init = true
    spellsync.init(json.encode(callback_ids), on_event_callback, function(self, message, callback_id)
        callback(decode_result(message))
    end)
end

---Execute SpellSync API function. If the API method is an object, additional methods (getters) can be listed in parameters.
---@param method string executed method of an object or field of an object, e.g. "player.sync"
---@param parameters any method parameters, if multiple parameters, then array of parameters
---@param callback function callback function, if nil, the function will immediately return the result
---@param native_api boolean if true, native method will be called
---@return any method execution result, or nil if callback is provided
function M.call_api(method, parameters, callback, native_api)
    if not is_init then
        error("Initialize SpellSync before calling functions!", 3)
    end
    if parameters == nil or (type(parameters) == "table" and #parameters == 0) then
        parameters = "[]"
    else
        if type(parameters) ~= "table" then
            parameters = { parameters }
        end
        parameters = json.encode(parameters)
    end
    if callback then
        spellsync.call_api(method, parameters, native_api, function(self, message)
            if message == "" then
                callback()
            else
                callback(decode_result(message))
            end
        end)
    else
        local result = spellsync.call_api(method, parameters, native_api)
        return decode_result(result)
    end
end

return M
