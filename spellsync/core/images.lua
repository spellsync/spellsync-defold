local M = {}
local core = require("spellsync.core.core")
local helpers = require("spellsync.core.helpers")
local callbacks = require("spellsync.core.callbacks")

---Upload image
---@param parameters table
---@param callback function callback function for handling the result of image upload: callback(image)
function M.upload(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("images.upload", { parameters }, callback)
end

---Upload image by URL
---@param parameters table
---@param callback function callback function for handling the result of image upload: callback(image)
function M.upload_url(parameters, callback)
    helpers.check_table_required(parameters)
    helpers.check_callback(callback)
    core.call_api("images.uploadUrl", { parameters }, callback)
end

---Choose file
---@param callback function callback function for handling the result of choosing an image: callback(result)
function M.choice_file(callback)
    helpers.check_callback(callback)
    core.call_api("images.chooseFile", nil, callback)
end

---Get images
---@param parameters table
---@param callback function callback function for handling the result of fetching images: callback(result)
function M.fetch(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("images.fetch", { parameters }, callback)
end

---Get more images
---@param parameters table
---@param callback function callback function for handling the result of fetching more images: callback(result)
function M.fetch_more(parameters, callback)
    helpers.check_table(parameters)
    helpers.check_callback(callback)
    core.call_api("images.fetchMore", { parameters }, callback)
end

---Resize image
---@param uri string image link
---@param width number required image width
---@param height number required image height
---@param crop boolean image cropping
---@return string returns link to the cropped image
function M.resize(uri, width, height, crop)
    helpers.check_string(uri, "uri")
    helpers.check_number(width, "width", true)
    helpers.check_number(height, "height", true)
    helpers.check_boolean(crop, "crop", true)
    return core.call_api("images.resize", { uri, width, height, crop })
end

---Check if image upload is possible
---@return boolean possibility to upload images
function M.can_upload()
    return core.call_api("images.canUpload") == true
end

M.callbacks = callbacks.images

return M
